from django.contrib import admin

from .models import Projet
# Register your models here.


@admin.register(Projet)
class BiblothequaireAdmin(admin.ModelAdmin):
    list_display = ('nomProjet','dateDebut', 'dateFin', 'nbrParticipants', 'numTel')
    list_filter = ('nomProjet','dateDebut', 'dateFin', 'nbrParticipants', 'numTel')
    search_fields = ('nomProjet','dateDebut', 'dateFin', 'nbrParticipants', 'numTel')