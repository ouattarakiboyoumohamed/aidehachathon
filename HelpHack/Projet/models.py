from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Projet(models.Model):
     
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, verbose_name="Utilisateur")
    nomProjet = models.CharField(max_length=150, verbose_name="Nom")
    dateDebut = models.DateField()
    dateFin = models.DateField()
    numTel = models.CharField(
        max_length=70, verbose_name="Numero de telephone")
    descriptionProjet = models.TextField(
        verbose_name="Description de l'hackathon")
    photo = models.ImageField(verbose_name=(
        "photo de l'hackathon"), blank=True, upload_to='projetphoto')
    participant = models.CharField(
        max_length=225, verbose_name="Qui participe ?")
    nbrParticipants = models.IntegerField()
    dateCreation = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.nomEvernement
