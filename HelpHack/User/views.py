from django.shortcuts import redirect, render
from django.contrib.auth.models import User

from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required


# Create your views here.

from .models import Personne, ProfilePersonne




def inscription(request):
    if request.POST:
        user_email = request.POST['email']
        user_name = request.POST['nom']
        user_lastName = request.POST['prenom']
        user_password = request.POST['password1']
        user_numTéléphone = request.POST['numTéléphone']


        
        try:
            # if(User.objects.filter(username= user_username).count() != 0):
            user_obj = User.objects.create(
                username=user_email, last_name=user_name, email=user_email, first_name=user_lastName)
            user_obj.set_password(user_password)
            user_obj.save()

            # user_Personne = Personne()
            # user_Personne.user = user_obj

            # user_Personne.numTel = user_numTéléphone
            # user_Personne.save()

            return redirect('connexion')
           
        except:
            print("erreur in !!!")

    else:
        print("erreur !!!")
    return render(request, 'inscription.html')



def connexion(request):
    if request.POST:
        user_email = request.POST['email']
        user_password = request.POST['password']
        try:
            user_auth = authenticate(
                username=user_email, password=user_password)
            login(request, user_auth)
            return redirect('listHackathon')
        except:
            print("erreur 1 !!!")
    else:
        print('erreur 2 !!!')
    return render(request, 'login.html')




@login_required
def deconnexion(request):
    try:
        logout(request)
        return redirect('connexion')
    except:
        print("error")
    return "error"
