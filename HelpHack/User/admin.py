from django.contrib import admin

from .models import Personne, ProfilePersonne
# Register your models here.

@admin.register(Personne)
class PersonneAdmin(admin.ModelAdmin):
    list_display = ('user','numTel', 'adress')
    list_filter = ('user','numTel', 'adress')
    search_fields = ('user','numTel', 'adress')
    
    
    
@admin.register(ProfilePersonne)
class ProfilePersonneAdmin(admin.ModelAdmin):
    list_display = ('user','ecole', 'filiere', 'niveau')
    list_filter = ('user','ecole', 'filiere', 'niveau')
    search_fields = ('user','ecole', 'filiere', 'niveau')