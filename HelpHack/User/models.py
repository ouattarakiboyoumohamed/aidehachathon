from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Personne(models.Model):
    user = models.OneToOneField(User, on_delete=models.CASCADE, verbose_name="Utilisateur")
    dateNaissance = models.DateField(blank=True)
    numTel = models.CharField(
        max_length=32, verbose_name="Numero de telephone", blank=True)
    adress = models.CharField(
        max_length=30, verbose_name="Lieu d'habitation", blank=True)
    photo = models.ImageField(verbose_name=("photo de profile"), blank=True)
    dateCreation = models.DateField(auto_now_add=True)
    
    def __str__(self):
        return self.user.first_name
    



class ProfilePersonne(models.Model):
    user = models.OneToOneField(
        User, on_delete=models.CASCADE, verbose_name="Utilisateur")
    ecole = models.CharField(max_length=255, verbose_name="Ecole/Universite")
    filiere = models.CharField(max_length=150, verbose_name="Filière")
    niveau = models.CharField(max_length=5, verbose_name=("Niveau"))

    def __str__(self):
        return self.user.first_name