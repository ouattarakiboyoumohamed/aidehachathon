import django_filters

from .models import Hackathon


class HackathonFiltre(django_filters.FilterSet):
    class Meta:
        model = Hackathon
        fields = ['nomEvernement',]
