from django.db import models
from django.contrib.auth.models import User

# Create your models here.


class Hackathon(models.Model):
    STATUS_CHOICES = [
        ("A Venir", 'A Venir'),
        ("Ouvert", 'Ouvert'),
        ("Fermé", 'Fermé'),
     ]
    
    EMPLACEMENT_CHOICES = [
        ("En ligne", 'En ligne'),
        ("En presentiel", 'En presentiel'),
     ]
    
    user = models.ForeignKey(User, on_delete=models.CASCADE, verbose_name="Utilisateur")
    nomEvernement = models.CharField(max_length=150, verbose_name="Nom")
    dateDebut = models.DateField()
    dateFin = models.DateField()
    numTel = models.CharField(max_length=70, verbose_name="Numero de telephone")
    lieu = models.CharField(max_length=30, verbose_name="Lieu d'habitation")
    lienInscription = models.TextField(verbose_name="Lien d'inscription")
    descriptionHack = models.TextField(verbose_name="Description de l'hackathon")
    photo = models.ImageField(verbose_name=("photo de l'hackathon"), blank=True, upload_to='hackphoto')
    participant = models.CharField(max_length=225, verbose_name="Qui participe ?")
    emplacement = models.CharField(choices=EMPLACEMENT_CHOICES, max_length=225)
    status = models.CharField(choices=STATUS_CHOICES, max_length=225)
    nbrParticipants = models.IntegerField()
    dateCreation = models.DateField(auto_now_add=True)

    def __str__(self):
        return self.nomEvernement