from django.urls import path

from .views import listHackathon, myHackathon, addHackathon, hackathonDetail


urlpatterns = [
    path('', listHackathon, name="listHackathon"),
    path('myHackathon/<int:pk>', myHackathon, name="myHackathon"),
    path('addHackathon/', addHackathon, name="addHackathon"),
    path('hackathonDetail/<int:pk>', hackathonDetail, name="hackathonDetail"),
   
]