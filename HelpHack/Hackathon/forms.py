from django import forms
from .models import Hackathon


class PostFormHackathon(forms.ModelForm):
    class Meta:
        model = Hackathon
        fields = '__all__'