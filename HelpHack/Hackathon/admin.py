from django.contrib import admin

from .models import Hackathon
# Register your models here.


@admin.register(Hackathon)
class BiblothequaireAdmin(admin.ModelAdmin):
    list_display = ('nomEvernement','dateDebut', 'dateFin', 'emplacement', 'status')
    list_filter = ('nomEvernement','dateDebut', 'dateFin', 'emplacement', 'status')
    search_fields = ('nomEvernement','dateDebut', 'dateFin', 'emplacement', 'status')