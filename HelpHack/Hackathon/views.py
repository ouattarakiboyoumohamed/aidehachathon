from django.shortcuts import render, redirect

from .forms import PostFormHackathon
from .models import Hackathon
from User.models import Personne

from django.core.paginator import Paginator, EmptyPage, PageNotAnInteger
# Create your views here.
from .filtreHack import HackathonFiltre


def listHackathon(request):
    hackathon = Hackathon.objects.all()
    
    paginator = Paginator(hackathon, per_page=4)
    pageNum = request.GET.get('page')

    hackathonFiltre = HackathonFiltre(request.GET, queryset=hackathon)
    hackathonlist = hackathonFiltre.qs

    if hackathonlist.count() >= 4:
        hackathonlist = paginator.get_page(pageNum)

    return render(request, 'hackathon.html', context={'hackathonlist': hackathonlist, 'paginator': paginator, 'pageNum': pageNum, 'hackathonFiltre': hackathonFiltre})



def myHackathon(request, pk):
    user = Personne.objects.get(user=pk)
    # hackathon = Hackathon.objects.filter(user=user).all()
    # context = {'hackathon': hackathon}
    return render(request, 'myhackathon.html')




def addHackathon(request):
    form = PostFormHackathon()
    if request.method == 'POST':
        form = PostFormHackathon(request.POST)
        if form.is_valid():
            form.save()
            return redirect('listHackathon')
        else:
            print(TypeError)
    
    return render(request, 'ajouthackathon.html', context={'form': form})



def hackathonDetail(request, pk):
    try:
        hackathon = Hackathon.objects.get(id=pk)
    except:
        print("error")
    return render(request, 'descriptionHack.html', context={'hackathon': hackathon})
